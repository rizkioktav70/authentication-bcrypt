# Binar: Authentication & Bcrypt

Di dalam repository ini terdapat implementasi authentication dan penggunaan `bcryptjs` di dalamnya.

## Getting Started

Untuk mulai membuat sebuah implementasi dari HTTP Server, mulainya menginspeksi file [`app/index.js`](./app/index.js), dan lihatlah salah satu contoh `controller` yang ada di [`app/controllers/mainController.js`](./app/controllers/mainController.js)

Lalu untuk menjalankan server, kalian tinggal jalanin salah satu script di package.json, yang namanya `start`.

```sh
npm run start
```

## Database Management

Di dalam repository ini sudah terdapat beberapa script yang dapat digunakan dalam memanage database, yaitu:

- `npx sequelize db:create` digunakan untuk membuat database
- `npx sequelize db:drop` digunakan untuk menghapus database
- `npx sequelize db:migrate` digunakan untuk menjalankan database migration
- `npx sequelize db:seed` digunakan untuk melakukan seeding
- `npx sequelize db:rollback` digunakan untuk membatalkan migrasi terakhir

# Output in Postman 

![screenshot](./screenshot/regis.png)
![screenshot](./screenshot/login.png)


## Endpoints

Di dalam repository ini, terdapat dua endpoint utama, yaitu `login` dan `register`.

##### `/api/v1/login`

Endpoint ini digunakan untuk login

###### Request

```json
{
  "email":"bangoktav70@gmail.com",
  "password":"babangganteng"
}
```

###### Response

```json
{
  "id": 3,
  "email": "bangoktav70@gmail.com",
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MywiZW1haWwiOiJiYW5nb2t0YXY3MEBnbWFpbC5jb20iLCJpYXQiOjE2NTIzMDE5NjN9.qfRbGdkEQXMq1cvl8Yx_lFiIZ82tl3_J13zpulKde2U",
  "createdAt": "2022-05-11T20:45:38.095Z",
  "updatedAt": "2022-05-11T20:45:38.095Z"
}
```

##### `/api/v1/register`

Endpoint ini digunakan untuk registrasi user

###### Request

```json
{
  "email":"bangoktav70@gmail.com",
  "password":"babangganteng"
}
```

###### Response

```json
{
  "id": 3,
  "email": "bangoktav70@gmail.com",
  "createdAt": "2022-05-11T20:45:38.095Z",
  "updatedAt": "2022-05-11T20:45:38.095Z"
}
```
